package com.zbm.model;

public class Capteur {
	private int indexSante;

	public Capteur() {
		this.setIndexSanteRandom();
	}
	
	public int getIndexSante() {
		return indexSante;
	}

	public void setIndexSanteRandom() {
		this.indexSante = (int) (Math.random() * 100) + 1;
	}
}
