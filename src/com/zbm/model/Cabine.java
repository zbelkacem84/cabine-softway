package com.zbm.model;

public class Cabine {

	public Capteur capteur;
	public String pathologie;
	public int indexSante;



	public Cabine() {
		super();
		this.capteur = new Capteur();
		this.setIndexSante(this.capteur.getIndexSante());
	}



	public String getPathologie() {
		try {
			StringBuilder sb = new StringBuilder("Pour l'index de santé "+this.getIndexSante()+",vous devez vous diriger en ");
			if(this.getIndexSante()%5 == 0 && this.getIndexSante()%3 == 0 ) {
				sb.append(UniteMedicale.CARDIOLOGIE.name()+", "+UniteMedicale.TRAUMATOLOGIE.name());
			}
			else if ((this.getIndexSante()%3) == 0) {
				sb.append(UniteMedicale.CARDIOLOGIE.name());
			}else if ((this.getIndexSante()%5) == 0) {
				sb.append(UniteMedicale.TRAUMATOLOGIE.name());
			}else {
				throw new Exception();
			}
			return sb.toString();
		} catch (Exception e) {
			return "Cas non défini";
		}

	}

	public void setPathologie(String pathologie) {
		this.pathologie = pathologie;
	}

	public int getIndexSante() {
		return indexSante;
	}

	public void setIndexSante(int indexSante) {
		this.indexSante = indexSante;
	}

	public Capteur getCapteur() {
		return capteur;
	}

	public void setCapteur(Capteur capteur) {
		this.capteur = capteur;
	}

}
