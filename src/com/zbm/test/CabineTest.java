package com.zbm.test;

import org.junit.jupiter.api.Test;

import com.zbm.model.Cabine;
import com.zbm.model.Capteur;

import junit.framework.TestCase;

public class CabineTest extends TestCase {
	
	@Test
	public void testGetPathologieCardiologie() {
		Cabine cabine = new Cabine();
		cabine.setIndexSante(33);
		assertEquals("Pour l'index de santé 33,vous devez vous diriger en CARDIOLOGIE", cabine.getPathologie());
	}
	
	@Test
	public void testGetPathologieTraumatologie() {
		Cabine cabine = new Cabine();
		cabine.setIndexSante(55);
		assertEquals("Pour l'index de santé 55,vous devez vous diriger en TRAUMATOLOGIE", cabine.getPathologie());
	}
	
	@Test
	public void testGetPathologieCardiologieTraumatologie() {
		Cabine cabine = new Cabine();
		cabine.setIndexSante(30);
		assertEquals("Pour l'index de santé 30,vous devez vous diriger en CARDIOLOGIE, TRAUMATOLOGIE", cabine.getPathologie());
	}
}
